FROM docker.io/nginx:1.27-alpine-slim

COPY nginx/minio-proxy.conf /etc/nginx/nginx.conf
